#include <cassert>

template <class State, class Successors>
BreadthFirstSearch<State, Successors>::BreadthFirstSearch( )
  : m_Allocator(), m_OpenList(), m_SuccessorsOf(), m_pNodePath(0)
{
}

template <class State, class Successors>
BreadthFirstSearch<State, Successors>::~BreadthFirstSearch( )
{
}

   //1. Put the root node on the queue.
   //2. Pull a node from the beginning of the queue and examine it.
   //       * If the searched element is found in this node, quit the search and return a result.
   //       * Otherwise push all the (so-far-unexamined) successors (the direct child nodes) of this node into the end of the queue, if there are any.
   //3. If the queue is empty, every node on the graph has been examined -- quit the search and return "not found".
   //4. Repeat from Step 2.


template <class State, class Successors>
bool BreadthFirstSearch<State, Successors>::find( const State &start, const State &end )
{
	//cleanup( );
	assert( m_OpenList.empty( ) == true );

	nAllocations = 0;

	// put the start node in the queue...
	Node *pStartNode = m_Allocator.allocate( 1 );
	nAllocations++;
	m_Allocator.construct( pStartNode, Node(start, NULL) );
	m_OpenList.push_back( pStartNode );

	while( !m_OpenList.empty( ) )
	{
		Node *pCurrentNode = m_OpenList.front( );
		//int size = m_VisitedNodes.size( );
		m_VisitedNodes.insert( pair<State, Node *>(pCurrentNode->state, pCurrentNode) );
		//assert( m_VisitedNodes.size( ) == (size + 1) );
		m_OpenList.pop_front( );


		if( pCurrentNode->state == end )
		{
			m_pNodePath = pCurrentNode;
			return true;
		}

		SuccessorCollection &successors = m_SuccessorsOf( pCurrentNode->state );
		SuccessorCollection::iterator itr;

		// for each successor...
		for( itr = successors.begin( ); itr != successors.end( ); ++itr )
		{
			VisitedCollection::const_iterator visitedItr = m_VisitedNodes.find( *itr );
			
			if( visitedItr == m_VisitedNodes.end( ) ) 
			{	
				Node *pNewNode = m_Allocator.allocate( 1 );
				nAllocations++;

				// calculate the cost and place in the queue...
				m_Allocator.construct( pNewNode, Node( *itr, pCurrentNode ) );
				m_OpenList.push_back( pNewNode );
			}
		}
	}

	return false;
}


template <class State, class Successors>
void BreadthFirstSearch<State, Successors>::cleanup( )
{
	// free memory on the queue...
	while( !m_OpenList.empty( ) )
	{
		Node *pCurrentNode = m_OpenList.front( );
		m_OpenList.pop_front( );

		m_Allocator.destroy( pCurrentNode );
		m_Allocator.deallocate( pCurrentNode, 1 );
		nAllocations--;
	}

	VisitedCollection::iterator itr;

	for( itr = m_VisitedNodes.begin( ); itr != m_VisitedNodes.end( ); ++itr )
	{
		m_Allocator.destroy( itr->second );
		m_Allocator.deallocate( itr->second, 1 );
		nAllocations--;
	}

	//assert( nAllocations == 0 );
	m_VisitedNodes.clear( );
	m_pNodePath = NULL; // reset to NULL...
}