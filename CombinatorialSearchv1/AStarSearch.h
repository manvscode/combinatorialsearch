#pragma once
#ifndef _ASTARSEARCH_H_
#define _ASTARSEARCH_H_
/*
 *	AStarSearch.h
 *
 *	A* search algorithm.
 *	Coded by Joseph A. Marrero, September 14, 2008
 *	http://www.ManVsCode.com/
 */
#include <memory>
#include <algorithm>
#include <vector>
#include <map>
#include <unordered_map> //tr1

namespace CombinatorialSearch {

template <class State, class Heuristic, class Cost, class Successors, class StateHasher = std::tr1::hash<State> >
class AStarSearch
{  
  public:
	typedef std::vector<State> SuccessorCollection;

	class Node
	{
	  public:
		const State state;
		int h; // hueristic
		int g; // cost
		int f;
		Node *parent;

		Node( const State &s ) : state(s), h(0), parent(NULL) {}
		Node( const State &s, int h_, int g_, Node *parent_ ) : state(s), h(h_), g(g_), f(h + g), parent(parent_) {}
	};



  protected:
	struct NodeLessThan {
		bool operator()( const Node *n1, const Node *n2 ) const
		{ return n1->f > n2->f; }
	};
	
	typedef std::vector<Node *> OpenCollection;
	typedef std::tr1::unordered_map<State, Node *, StateHasher> OpenHashMap;
	typedef std::map<State, Node *> ClosedCollection;
	NodeLessThan m_FComparer;

  private:	
	std::allocator<Node> m_Allocator;
	OpenCollection m_OpenList; 
	OpenHashMap m_OpenHashMap;
	ClosedCollection m_ClosedList;
	Heuristic m_HeuristicFunction;
	Cost m_CostFunction;
	Successors m_SuccessorsOf;
	Node *m_pNodePath;

#ifdef _ASTARSEARCH_MEMORY_DEBUG
	int nAllocations;
#endif

  public:
	AStarSearch( );
	~AStarSearch( );


	bool find( const State &start, const State &end );
	Node *getPath( );
	void cleanup( );

  protected:
	Node *openListBestCandidate( );
	void openListPush( Node *&t );
	void openListPop( );
	void openListHeapify( );
	Node *openListFind( State s );
};

template <class State, class Heuristic, class Cost, class Successors, class StateHasher>
inline typename AStarSearch<State, Heuristic, Cost, Successors, StateHasher>::Node *AStarSearch<State, Heuristic, Cost, Successors, StateHasher>::getPath( )
{ return m_pNodePath; }


template <class State, class Heuristic, class Cost, class Successors, class StateHasher>
inline typename AStarSearch<State, Heuristic, Cost, Successors, StateHasher>::Node *AStarSearch<State, Heuristic, Cost, Successors, StateHasher>::openListBestCandidate( )
{ return m_OpenList.front( ); }


template <class State, class Heuristic, class Cost, class Successors, class StateHasher>
inline void AStarSearch<State, Heuristic, Cost, Successors, StateHasher>::openListPush( Node *&t )
{
	m_OpenList.push_back( t );
	push_heap( m_OpenList.begin( ), m_OpenList.end( ), m_FComparer );

	m_OpenHashMap[ t->state ] = t; 
}

template <class State, class Heuristic, class Cost, class Successors, class StateHasher>
inline void AStarSearch<State, Heuristic, Cost, Successors, StateHasher>::openListPop( )
{
	Node *pNode = m_OpenList.front( );
	pop_heap( m_OpenList.begin( ), m_OpenList.end( ), m_FComparer );
	m_OpenList.pop_back( );

	m_OpenHashMap.erase( pNode->state );
}

template <class State, class Heuristic, class Cost, class Successors, class StateHasher>
inline void AStarSearch<State, Heuristic, Cost, Successors, StateHasher>::openListHeapify( )
{ std::make_heap( m_OpenList.begin( ), m_OpenList.end( ), m_FComparer ); }



template <class State, class Heuristic, class Cost, class Successors, class StateHasher>
inline typename AStarSearch<State, Heuristic, Cost, Successors, StateHasher>::Node *AStarSearch<State, Heuristic, Cost, Successors, StateHasher>::openListFind( State s )
{
	OpenHashMap::iterator itr = m_OpenHashMap.find( s );

	if( itr == m_OpenHashMap.end( ) )
	{
		return NULL;
	}
	else
	{
		return itr->second;
	}
}

#include "AStarSearch.inl"
} // end of namespace
#endif
