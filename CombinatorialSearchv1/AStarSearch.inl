/*
 *	AStarSearch.inl
 *
 *	A* search algorithm.
 *	Coded by Joseph A. Marrero, September 14, 2008
 *	http://www.ManVsCode.com/
 */
#include <cassert>

template <class State, class Heuristic, class Cost, class Successors, class StateHasher>
AStarSearch<State, Heuristic, Cost, Successors, StateHasher>::AStarSearch( )
  : m_Allocator(), 
    m_OpenList(), 
	m_OpenHashMap(),
	m_ClosedList(),
	m_HeuristicFunction(), 
	m_SuccessorsOf(), 
	m_pNodePath(0)
{
}

template <class State, class Heuristic, class Cost, class Successors, class StateHasher>
AStarSearch<State, Heuristic, Cost, Successors, StateHasher>::~AStarSearch( )
{
}


template <class State, class Heuristic, class Cost, class Successors, class StateHasher>
bool AStarSearch<State, Heuristic, Cost, Successors, StateHasher>::find( const State &start, const State &end )
{
	//cleanup( );
	assert( m_OpenList.empty( ) == true && m_ClosedList.empty( ) == true );

#ifdef _ASTARSEARCH_MEMORY_DEBUG
	nAllocations = 0;
#endif

	// put the start node in the queue...
	Node *pStartNode = m_Allocator.allocate( 1 );
#ifdef _ASTARSEARCH_MEMORY_DEBUG
	nAllocations++;
#endif
	m_Allocator.construct( pStartNode, Node( start, m_HeuristicFunction( start, end ), 0 /* no cost */, NULL ) );
	openListPush( pStartNode );

	while( !m_OpenList.empty( ) )
	{
		Node *pCurrentNode = openListBestCandidate( );

		if( pCurrentNode->state == end )
		{
			m_pNodePath = pCurrentNode;
			return true;
		}
		
		m_ClosedList.insert( pair<State, Node *>(pCurrentNode->state, pCurrentNode) );
		openListPop( );


		SuccessorCollection &successors = m_SuccessorsOf( pCurrentNode->state );
		SuccessorCollection::iterator itr;

		// for each successor...
		for( itr = successors.begin( ); itr != successors.end( ); ++itr )
		{
			//assert( *itr != pCurrentNode->state );
			ClosedCollection::iterator visitedItr = m_ClosedList.find( *itr );
			


			if( visitedItr == m_ClosedList.end( ) ) // not visited
			{
				Node *pExistingOpenNode = openListFind( *itr );

				if( pExistingOpenNode == NULL ) // not on open list
				{
					Node *pNewNode = m_Allocator.allocate( 1 );
					
					#ifdef _ASTARSEARCH_MEMORY_DEBUG
					nAllocations++;
					#endif

					int h = m_HeuristicFunction( *itr, end );
					int g = pCurrentNode->g + m_CostFunction( pCurrentNode->state, *itr );

					// calculate the cost and place in the queue...
					m_Allocator.construct( pNewNode, Node( *itr, h, g, pCurrentNode ) );					
					openListPush( pNewNode );
				}
				else // already on open list
				{
					int h = m_HeuristicFunction( pExistingOpenNode->state, end );
					int g = pCurrentNode->g + m_CostFunction( pCurrentNode->state, pExistingOpenNode->state );
					int f = h + g;

					if( f < pExistingOpenNode->f )
					{	
						pExistingOpenNode->h = h;
						pExistingOpenNode->g = g;
						pExistingOpenNode->f = f;
						pExistingOpenNode->parent = pCurrentNode;
						openListHeapify( );
					}
				}
			}
			#ifdef DO_NOT_SUPPORT_NEGATIVE_WEIGHTS
				// do nothing intentionally; this is an optimization!
			#else
			else
			{
				int h = m_HeuristicFunction( visitedItr->first, end );
				int g = pCurrentNode->g + m_CostFunction( pCurrentNode->state, visitedItr->first );
				int f = h + g;

				if( f < visitedItr->second->f )
				{
					// ok, m_CostFunction() returned a negative weight here, so
					// that means our new f was better than the old f.
					visitedItr->second->h = h;
					visitedItr->second->g = g;
					visitedItr->second->f = f;	
					visitedItr->second->parent = pCurrentNode;

					openListPush( visitedItr->second ); 
					m_ClosedList.erase( visitedItr );
				}
			}
			#endif
		}
	}

	return false;
}


template <class State, class Heuristic, class Cost, class Successors, class StateHasher>
void AStarSearch<State, Heuristic, Cost, Successors, StateHasher>::cleanup( )
{
#ifdef _ASTARSEARCH_MEMORY_DEBUG
	assert( nAllocations == m_OpenList.size( ) + m_ClosedList.size( ) );
#endif

	// free memory on the queue...
	for( OpenCollection::iterator itr = m_OpenList.begin( );
		 itr != m_OpenList.end( );
		 ++itr )
	{
		m_Allocator.destroy( *itr );
		m_Allocator.deallocate( *itr, 1 );

#ifdef _ASTARSEARCH_MEMORY_DEBUG
		nAllocations--;
#endif
	}
	m_OpenList.clear( );

	for( ClosedCollection::iterator itr = m_ClosedList.begin( );
		 itr != m_ClosedList.end( );
		 ++itr )
	{
		m_Allocator.destroy( itr->second );
		m_Allocator.deallocate( itr->second, 1 );
#ifdef _ASTARSEARCH_MEMORY_DEBUG
		nAllocations--;
#endif
	}

	m_ClosedList.clear( );
	m_OpenHashMap.clear( );
	assert( m_OpenList.empty( ) == true && m_ClosedList.empty( ) == true );
	m_pNodePath = NULL; // reset to NULL...
}
