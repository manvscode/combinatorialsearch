#pragma once
#ifndef _BREADTHFIRSTSEARCH_H_
#define _BREADTHFIRSTSEARCH_H_

#include <memory>
#include <list>
#include <map>


template <class State, class Successors>
class BreadthFirstSearch
{  
  public:
	typedef std::vector<State> SuccessorCollection;

	class Node
	{
	  public:
		const State state;
		Node *parent;

		Node( const State &s ) : state(s), parent(NULL) {}
		Node( const State &s,Node *parent_ ) : state(s), parent(parent_) {}
	};
	
  private:	
	typedef std::list<Node *> NodeCollection;
	typedef std::map<State, Node *> VisitedCollection;

  private:	
	std::allocator<Node> m_Allocator;
	NodeCollection m_OpenList; // open list
	VisitedCollection m_VisitedNodes;
	Successors m_SuccessorsOf;
	Node *m_pNodePath;

	int nAllocations;


  public:
	BreadthFirstSearch( );
	~BreadthFirstSearch( );


	bool find( const State &start, const State &end );
	Node *getPath( );
	void cleanup( );
};

template <class State, class Successors>
inline typename BreadthFirstSearch<State, Successors>::Node *BreadthFirstSearch<State, Successors>::getPath( )
{ return m_pNodePath; }


#include "BreadthFirstSearch.inl"
#endif