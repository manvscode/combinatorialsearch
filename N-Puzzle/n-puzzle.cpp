/*
 *	n-puzzle.cpp
 *
 *	A N-Puzzle solver. See the wikipedia article on 
 *	the N-Puzzle (or 15-puzzle) for more information.
 *	http://en.wikipedia.org/wiki/Fifteen_puzzle
 *
 *	Coded by Joseph A. Marrero
 *	http://www.ManVsCode.com/
 */
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <string>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include "n-puzzle.h"
using namespace std;


ManhattanDistance manhattanDistance;
StateCollection generatedStates;

int main( int argc, char *argv[] )
{
	cout << PUZZLE_SIZE - 1 << "-Puzzle" << endl;
	cout << "==============" << endl;
	cout << endl;


	Puzzle p;

	p.print( cout, "Initial State" );
	Solver solver;

	if( solver.find( p, Puzzle::GOAL_A ) )
	{
		//uint n = 1;
		//Solver::Node *pNode = solver.getPath( );
		//
		//while( pNode != NULL )
		//{
		//	ostringstream oss;
		//	oss << "Step " << n++;
		//	pNode->state.print( cout, oss.str( ).c_str( ) );
		//	pNode = pNode->parent;
		//}
		
		solver.cleanup( );
	}

	if( solver.find( p, Puzzle::GOAL_B ) )
	{
		//uint n = 1;
		//Solver::Node *pNode = solver.getPath( );
		//
		//while( pNode != NULL )
		//{
		//	ostringstream oss;
		//	oss << "Step " << n++;
		//	pNode->state.print( cout, oss.str( ).c_str( ) );
		//	pNode = pNode->parent;
		//}
		
		solver.cleanup( );
	}
	return 0;
}

Puzzle Puzzle::GOAL_A = goalA( );
Puzzle Puzzle::GOAL_B = goalB( );



Puzzle Puzzle::goalA( )
{
	Puzzle p;

	p.state[ 0 ][ 0 ] = 0;
	p.state[ 0 ][ 1 ] = 1;
	p.state[ 0 ][ 2 ] = 2;
	p.state[ 1 ][ 0 ] = 3;
	p.state[ 1 ][ 1 ] = 4;
	p.state[ 1 ][ 2 ] = 5;
	p.state[ 2 ][ 0 ] = 6;
	p.state[ 2 ][ 1 ] = 7;
	p.state[ 2 ][ 2 ] = 8;
	
	p.pivotPoint.first = 0;
	p.pivotPoint.second = 0;

	return p;
}

Puzzle Puzzle::goalB( )
{
	Puzzle p;
	
	p.state[ 0 ][ 0 ] = 1;
	p.state[ 0 ][ 1 ] = 2;
	p.state[ 0 ][ 2 ] = 3;
	p.state[ 1 ][ 0 ] = 8;
	p.state[ 1 ][ 1 ] = 0;
	p.state[ 1 ][ 2 ] = 4;
	p.state[ 2 ][ 0 ] = 7;
	p.state[ 2 ][ 1 ] = 6;
	p.state[ 2 ][ 2 ] = 5;
	
	p.pivotPoint.first = 1;
	p.pivotPoint.second = 1;

	return p;
}

Puzzle::Puzzle( )
{
	for( int r = 0; r < PUZZLE_DIMENSION; r++ )
	{
		for( int c = 0; c < PUZZLE_DIMENSION; c++ )
		{
			state[ r ][ c ] = r * PUZZLE_DIMENSION + c;
		}
	}

	pivotPoint.first = 0;
	pivotPoint.second = 0;
	scrambble( );
}

Puzzle::Puzzle( const Puzzle &other )
{
	for( int r = 0; r < PUZZLE_DIMENSION; r++ )
	{
		for( int c = 0; c < PUZZLE_DIMENSION; c++ )
		{
			state[ r ][ c ] = other[ r ][ c ];
		}
	}

	pivotPoint = other.pivotPoint;
}


void Puzzle::scrambble( )
{	
	srand( time(0) );

	for( int i = 0; i < PUZZLE_SIZE; i++ )
	{
		int r1 = rand( ) % PUZZLE_DIMENSION;
		int r2 = rand( ) % PUZZLE_DIMENSION;
		int c1 = rand( ) % PUZZLE_DIMENSION;
		int c2 = rand( ) % PUZZLE_DIMENSION;

		if( state[ r1 ][ c1 ] == 0 )
		{
			pivotPoint.first = r2;
			pivotPoint.second = c2;
		}
		else if( state[ r2 ][ c2 ] == 0 )
		{
			pivotPoint.first = r1;
			pivotPoint.second = c1;
		}

		std::swap( state[ r1 ][ c1 ], state[ r2 ][ c2 ] );
	}
}

uint &Puzzle::operator()( int i, int j )
{ 
	assert( i < PUZZLE_DIMENSION );
	assert( j < PUZZLE_DIMENSION );
	return state[ i ][ j ];
}

const uint *Puzzle::operator[]( int i ) const
{ 
	assert( i < PUZZLE_DIMENSION );
	return state[ i ];
}


Puzzle::MoveCollection Puzzle::moves( ) const
{
	MoveCollection result;


	for( int i = -1; i <= 1; i += 2 )
	{
		int x = pivotPoint.first + i;
		if( x < 0 || x >= PUZZLE_DIMENSION )
			continue;
		result.push_back( pair<uint, uint>( x, pivotPoint.second ) );
	}

	for( int i = -1; i <= 1; i += 2 )
	{
		int y = pivotPoint.second + i;
		if( y < 0 || y >= PUZZLE_DIMENSION )
			continue;
		result.push_back( pair<uint, uint>( pivotPoint.first, y ) );
	}

	return result;
}

void Puzzle::move( uint x, uint y )
{	
	assert( abs(pivotPoint.first - x) == 1 || abs(pivotPoint.second - y) == 1 );
	swap( state[ pivotPoint.first ][ pivotPoint.second ], state[ x ][ y ] );
}

void Puzzle::print( ostream &out, const char *label ) const
{
	for( int r = 0; r < PUZZLE_DIMENSION; r++ )
	{
		if( r == PUZZLE_DIMENSION / 2 )		
			out << ' ' << setfill( ' ' ) << setw( 15 ) << left << label << right << "|";
		else
			out << ' ' << setfill( ' ' ) << setw( 15 ) << ' ' << right << "|";

		for( int c = 0; c < PUZZLE_DIMENSION; c++ )
		{
			if( state[ r ][ c ] != 0 )
				out << setw( 2 ) << setfill( ' ' ) << state[ r ][ c ] << ' ';
			else
				out << setw( 2 ) << setfill( ' ' ) << ' ' << ' ';
		}
		out << "|" << endl;
	}
	out << endl;
}

bool Puzzle::operator==( const Puzzle &rhs ) const
{
	for( int r = 0; r < PUZZLE_DIMENSION; r++ )
	{
		for( int c = 0; c < PUZZLE_DIMENSION; c++ )
		{
			if( state[ r ][ c ] != rhs.state[ r ][ c ] )
				return false;
		}
	}

	return true;
}


bool Puzzle::operator<( const Puzzle &rhs ) const
{
	static Cost c;
	return c( *this, Puzzle::GOAL_A ) < c( rhs, Puzzle::GOAL_A );
}