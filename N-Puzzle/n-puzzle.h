#ifndef _8PUZZLE_H_
#define _8PUZZLE_H_
/*
 *	n-puzzle.h
 *
 *	A N-Puzzle solver. See the wikipedia article on 
 *	the N-Puzzle (or 15-puzzle) for more information.
 *	http://en.wikipedia.org/wiki/Fifteen_puzzle
 *
 *	Coded by Joseph A. Marrero
 *	http://www.ManVsCode.com/
 */
#include <CombinatorialSearch/AStarSearch.h>
#include <CombinatorialSearch/BestFirstSearch.h>
#include <iostream>
#include <vector>
#include <utility>
using namespace std;

#define	PUZZLE_DIMENSION		3
#define PUZZLE_SIZE				(PUZZLE_DIMENSION * PUZZLE_DIMENSION)


typedef unsigned int uint;

typedef struct tagManhattanDistance {
	unsigned int operator()( unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2 ) const
	{ return (unsigned int) std::abs(x1 - x2) + std::abs(y1 - y2); }
} ManhattanDistance;


class Puzzle
{
  public:
	typedef std::vector< std::pair<uint, uint> > MoveCollection;

	static Puzzle GOAL_A;
	static Puzzle GOAL_B;

  protected:
	uint state[ PUZZLE_DIMENSION ][ PUZZLE_DIMENSION ];
	std::pair<uint, uint> pivotPoint;

	void scrambble( );

	static Puzzle goalA( );
	static Puzzle goalB( );

  public:
	Puzzle( );
	Puzzle( const Puzzle &other );
	uint &operator()( int i, int j );
	const uint *operator[]( int i ) const;
	MoveCollection moves( ) const;
	void move( uint x, uint y );
	void print( ostream &out, const char *label = NULL ) const;
	bool operator==( const Puzzle &rhs ) const;
	bool operator<( const Puzzle &rhs ) const;
};


extern ManhattanDistance manhattanDistance;


typedef struct tagCost {

	uint operator()( const Puzzle &p1, const Puzzle &p2 ) const
	{
		uint sum = 0;

		for( int r1 = 0; r1 < PUZZLE_DIMENSION; r1++ )
		{
			for( int c1 = 0; c1 < PUZZLE_DIMENSION; c1++ )
			{
				for( int r2 = 0; r2 < PUZZLE_DIMENSION; r2++ )
				{
					for( int c2 = 0; c2 < PUZZLE_DIMENSION; c2++ )
					{
						if( p1[ r1 ][ c1 ] == p2[ r2 ][ c2 ] )
							sum += manhattanDistance( r1, c1, r2, c2 );
					}
				}
			}
		}

		return sum;
	}
} Cost;

typedef struct tagHeuristic {

	uint operator()( const Puzzle &p1, const Puzzle &p2 ) const
	{
		uint sum = 0;

		for( int r1 = 0; r1 < PUZZLE_DIMENSION; r1++ )
		{
			for( int c1 = 0; c1 < PUZZLE_DIMENSION; c1++ )
			{
				for( int r2 = 0; r2 < PUZZLE_DIMENSION; r2++ )
				{
					for( int c2 = 0; c2 < PUZZLE_DIMENSION; c2++ )
					{
						if( p1[ r1 ][ c1 ] == p2[ r2 ][ c2 ] )
							sum += manhattanDistance( r1, c1, r2, c2 );
					}
				}
			}
		}

		return sum;
	}
} Heuristic;

typedef struct tagPuzzleHasher {
	Cost theCost;
	unsigned int operator()( const Puzzle &p ) const
	{
		//unsigned int bits = 0;
		//short n = 0;

		//for( int r1 = 0; r1 < PUZZLE_DIMENSION; r1++ )
		//{
		//	for( int c1 = 0; c1 < PUZZLE_DIMENSION; c1++ )
		//	{
		//		bits |= (( p[ r1 ][ c1 ] + r1 * c1 ) % 2) << n + 9;
		//		bits |= (p[ r1 ][ c1 ] % 2) << n++;
		//	}
		//}
		//return bits;
		return theCost( p, Puzzle::GOAL_A );
	}
} PuzzleHasher;

typedef std::vector<Puzzle> StateCollection;
extern StateCollection generatedStates;

typedef struct tagNextStates {
	StateCollection operator()( const Puzzle &p ) const
	{
		StateCollection result;
		Puzzle::MoveCollection &moves = p.moves( );
		Puzzle::MoveCollection::const_iterator itr;

		for( itr = moves.begin( ); itr != moves.end( ); ++itr )
		{
			Puzzle newPuzzle( p );
			newPuzzle.move( itr->first, itr->second );
			result.push_back( newPuzzle );
			//generatedStates.push_back( newPuzzle );
		}
		return result;
	}
} NextStates;


typedef CombinatorialSearch::AStarSearch<Puzzle, Cost, Heuristic, NextStates, PuzzleHasher> Solver;



#endif