#ifndef _GRAPH_H_
#define _GRAPH_H_
#include <vector>
#include <set>
//////////////////////////////////////////////////////////////////
/////////////// GENERIC GRAPH DATA STRUCTURE /////////////////////
//////////////////////////////////////////////////////////////////
template <typename Type>
class Graph
{
  public:
	typedef unsigned int VertexName;
	typedef std::set<VertexName> EdgeCollection;
	static const int NO_PATH_LIMIT = -1;

	enum SearchAlgorithm { 
		BREADTH_FIRST_SEARCH = 0,
		DEPTH_FIRST_SEARCH,
		DIJKSTRA_SEARCH,
		BEST_FIRST_SEARCH,
		ASTAR_SEARCH,
		SEARCH_ALGORITHM_COUNT
	};

  protected:
	struct Vertex { 
		const VertexName name; // always guaranteed to be unique
		Type data; 
		EdgeCollection edges; // all edges are directed from this vertex to each vertex in this collection.
		int cost;
		Vertex( VertexName _name, const Type &_data );
	};
	typedef std::vector<Graph::Vertex> VertexCollection;
	////////////////////////////////
	//////// Member Variables //////
	////////////////////////////////
	VertexCollection m_Vertices;
	VertexCollection m_Path;
	int m_PathLengthLimit;

  public:
	Graph( );
	virtual ~Graph( );

	VertexName addVertex( const Type &t );
	bool       addDirectedEdge( VertexName fromVertex, VertexName toVertex );
	bool       addEdge( VertexName vertex, VertexName otherVertex );
	bool       search( VertexName start, VertexName end, SearchAlgorithm algorithm = BREADTH_FIRST_SEARCH );

	const EdgeCollection&   successors( VertexName vertex ) const;
	const VertexCollection& path( ) const;

protected:
	bool breadthFirstSearch( VertexName start, VertexName end );
	bool depthFirstSearch( VertexName start, VertexName end );
	bool dijkstraSearch( VertexName start, VertexName end );
};
	
template <typename Type>
inline const Graph<Type>::EdgeCollection& Graph<Type>::successors( VertexName vertex ) const
{
	return m_Vertices[ vertex ].edges;
}

template <typename Type>
inline const Graph<Type>::VertexCollection& Graph<Type>::path( ) const
{
	return m_Path;
}

#include "Graph.inl"
#endif // _GRAPH_H_
