#include <cassert>
#include <climits>
	
template <typename Type>
Graph<Type>::Graph( )
  : m_Vertices( )
{
}

template <typename Type>
Graph<Type>::~Graph( )
{
}

template <typename Type>
VertexName Graph<Type>::addVertex( const Type &t )
{
	Vertex v( m_Vertices.size( ), t );
	m_Vertices.push_back( v );
	return v.name;
}

template <typename Type>
bool Graph<Type>::addDirectedEdge( VertexName fromVertex, VertexName toVertex )
{
	Vertex &vertex = m_Vertices[ fromVertex ];
	return vertex.edges.insert( toVertex ).second;
}

template <typename Type>
bool Graph<Type>::addEdge( VertexName vertex, VertexName otherVertex )
{
	return addDirectedEdge( vertex, otherVertex ) && addDirectedEdge( otherVertex, vertex );	
}


template <typename Type>
bool Graph<Type>::search( VertexName start, VertexName end, SearchAlgorithm algorithm )
{
	assert( start != end ); // avoid the trivial case
	assert( m_Vertices.size( ) > 0 ); // Need at least one vertex

	switch( algorithm )
	{
		case BREADTH_FIRST_SEARCH:
			return breadthFirstSearch( start, end );
		case DEPTH_FIRST_SEARCH:
			return depthFirstSearch( start, end );
		case DIJKSTRA_SEARCH:
			return dijkstraSearch( start, end );
		case BEST_FIRST_SEARCH:
		case ASTAR_SEARCH:
		default:
			return false;
	}
}

template <typename Type>
bool Graph<Type>::breadthFirstSearch( VertexName start, VertexName end )
{
	std::list<VertexName> list;

	list.push_back( start );

	while( list.size( ) > 0 )
	{
		VertexName v = list.front( );
		list.pop_front( );

		if( v == end )
			return true;

		const EdgeCollection &edges = successors( v );	

		for( EdgeCollection::const_iterator itr = edges.begin( );
			 itr != edges.end( );
		     ++itr )
		{
			list.push_back( *itr );
		}		


	}

	return false;
}

template <typename Type>
bool Graph<Type>::depthFirstSearch( VertexName start, VertexName end )
{
	return false;
}

template <typename Type>
bool Graph<Type>::dijkstraSearch( VertexName start, VertexName end )
{
	return false;
}


