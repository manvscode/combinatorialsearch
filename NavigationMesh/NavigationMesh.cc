/*
 *	NavigationMesh.cc
 * 
 *	3D pathfinding using a navigation mesh.
 *
 *	Coded by Joseph A. Marrero
 *	1/17/2009
 */
#include <cmath>
#include <iostream>
#include <GL/freeglut/freeglut.h>
#include "NavigationMesh.h"
#include "OBJLoader.h"
#include "debug.h"
using namespace std;

unsigned int gridList = 0;
GLUquadric *pQuadric = NULL;


//int startNode = 50;
//int startNode = 40;
int endNode = 92;
#define COUNT 6
Entity *pEntity[ COUNT ];

int main( int argc, char *argv[] )
{
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE );

	glutInitWindowSize( 640, 480 );
	glutCreateWindow( "3D Pathfinding using a Navigation Mesh" );


	glutDisplayFunc( render );
	glutReshapeFunc( resize );
	glutKeyboardFunc( keyboard_keypress );
	glutIdleFunc( idle );


	glutSetOption( GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS );

	initialize( );
	glutMainLoop( );
	deinitialize( );

	return 0;
}


OBJLoader objLoader;
NavigationMesh *pNavMesh = NULL;

void initialize( )
{
	Debug::destroyConsole( );
	Debug::createConsole( );
	//glEnable( GL_LIGHTING );
	//glEnable( GL_LIGHT0 );
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
	glHint( GL_POINT_SMOOTH_HINT, GL_NICEST );
	glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
	glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
	glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
	
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	

	glShadeModel( GL_SMOOTH );
	//glClearDepth( 1.0f );		
	//glDepthFunc( GL_LEQUAL );
	//glEnable( GL_DEPTH_TEST );

	glPixelStorei( GL_PACK_ALIGNMENT, 4 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );

	gridList = glGenLists( 1 );
	glNewList( gridList, GL_COMPILE );
		glPushAttrib( GL_CURRENT_BIT | GL_LINE_BIT );			
			//glEnable( GL_LINE_STIPPLE );
			//glLineStipple( 1, 0xF0F0 );
			glLineWidth( 0.35f );
			glColor3f( 0.1f, 0.1f, 0.1f );

			glMapGrid2f( GRID_CELL_WIDTH, 0.0, 1.0, GRID_CELL_HEIGHT, 0.0, 1.0 );
			glEvalMesh2( GL_LINE, 0, GRID_CELL_WIDTH, 0, GRID_CELL_HEIGHT );
		glPopAttrib( );
	glEndList( );

	pNavMesh = NavigationMesh::instance( );

	// load in the navigation mesh...
	Debug::print( "Loading navigation mesh data...\n" );
	objLoader.load( "navigation_mesh.obj", true ); 
	//objLoader.load( "test.obj", true ); 

	// Tell OpenGL about the vertex data.
	const OBJLoader::OBJVertexCollection &vertexData = objLoader.vertices( );
	glInterleavedArrays( GL_V3F, 0, &vertexData[ 0 ] );

	// Use the raw vertex and face data to determine edges...
	const OBJLoader::OBJFaceCollection &theFaces = objLoader.faces( );


	Debug::print( "Adding polygons...\n" );
	for( unsigned int f = 0; f < theFaces.size( ); f++ )
	{
		NavigationMesh::Polygon p;
		int firstVertex         = -1;
		int previousVertexIndex = -1;

		for( int v = 0; v < theFaces[ f ].vertexIndices.size( ); v++ )
		{
			int vertexIndex = theFaces[ f ].vertexIndices[ v ];

			p.addVertex( vertexIndex );
			if( previousVertexIndex != -1 )
			{
				p.addEdge( previousVertexIndex, vertexIndex );
			}
			else
			{
				firstVertex = vertexIndex;
			}

			previousVertexIndex = vertexIndex;
		}
		p.addEdge( previousVertexIndex, firstVertex );
		
		pNavMesh->add( p );
	}

	
	Debug::print( "Initializing navigation mesh...\n" );
	pNavMesh->initialize( );
	
	pQuadric = gluNewQuadric( );

	for( int i = 0; i < COUNT; i++ )
		pEntity[ i ] = new Entity( );
}

void deinitialize( )
{
	// TO DO: Deinitialization code goes here...
	Debug::print( "Deinitializing...\n" );
	
	for( int i = 0; i < COUNT; i++ )
		delete pEntity[ i ];
	gluDeleteQuadric( pQuadric );
	delete pNavMesh;
}


void render( )
{
	int startTime = glutGet( GLUT_ELAPSED_TIME );
	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glLoadIdentity( );

	glRotatef( 90.0f, 1.0f, 0.0f, 0.0f );
	glTranslatef( -5.0f, -65.0f, 15.0f );
	//glRotatef( 25.0f, 1.0f, 0.0f, 0.0f );
	//glTranslatef( -5.0f, -25.0f, -45.0f );

	// draw the polygons in the navigation mesh...
	const OBJLoader::OBJVertexCollection &vertexData = objLoader.vertices( );

	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	for( int i = 0; i < pNavMesh->numberOfNodes( ); i++ )
	{ 
		if( i == endNode )
			glColor3f( 1.0f, 0.0f, 0.0f );
		else
		{
			glColor3f( 1.0f, 1.0f, 1.0f );
			for( int j = 0; j < COUNT; j++ )
			{
				if( i == pEntity[ j ]->startNode )
				{
					glColor3f( 0.0f, 1.0f, 0.0f );
					break;
				}
			}
		}



		const NavigationMesh::Polygon &p = pNavMesh->nodePolygon( i );
		const NavigationMesh::IndexCollection &indices = p.vertexIndices( );
		glDrawElements( GL_POLYGON, p.numberOfVertices( ), GL_UNSIGNED_INT, &indices[ 0 ] ); 

		/*
		glPushMatrix( );
			glColor3f( 1.0f, 1.0f, 0.0f );
			float x, z;
			pNavMesh->getNodeCenter( i, x, z );
			glTranslatef( x, 1.0f, z );

			gluSphere( pQuadric, 0.1f, 4, 4 );
		glPopMatrix( );
		*/
	}


	/*
	for( int j = 0; j < path.size( ); j++ )
	{
		glColor3f( 1.0f, 1.0f, 0.0f );
		const NavigationMesh::Polygon &p = pNavMesh->nodePolygon( path[ j ] );
		const NavigationMesh::IndexCollection &indices = p.vertexIndices( );
		glDrawElements( GL_POLYGON, p.numberOfVertices( ), GL_UNSIGNED_INT, &indices[ 0 ] ); 

		glPushMatrix( );
			glColor3f( 1.0f, 1.0f, 0.0f );
			float x, z;
			pNavMesh->getNodeCenter( path[ j ], x, z );
			glTranslatef( x, 1.0f, z );

			gluSphere( pQuadric, 0.1f, 4, 4 );
		glPopMatrix( );
	}
	*/
	

	for( int i = 0; i < COUNT; i++ )
	{
		pEntity[ i ]->render( );
	}

	/* Write text */
	int width = glutGet((GLenum)GLUT_WINDOW_WIDTH);
	int height = glutGet((GLenum)GLUT_WINDOW_HEIGHT);

	writeText( GLUT_BITMAP_HELVETICA_18, std::string("3D Pathfinding using a Navigation Mesh"), 2, 22 );
	writeText( GLUT_BITMAP_8_BY_13, std::string("Press <ESC> to quit."), 2, 5 );
	
	glutSwapBuffers( );


	// Updates
	float timeSlice = (glutGet( GLUT_ELAPSED_TIME ) - startTime) / 1000.0f;

	for( int i = 0; i < COUNT; i++ )
	{
		pEntity[ i ]->update( timeSlice );
	}
}

void resize( int width, int height )
{
	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );
	

	if( height == 0 )
		height = 1;
		
	//gluPerspective( 45.0f, (double) width / (double) height, 1.0, 200.0 );
	glOrtho( -50.0f, 50.0f, -50.0f, 50.0f,-100.0f, 100.0f );
	glMatrixMode( GL_MODELVIEW );
}

void keyboard_keypress( unsigned char key, int x, int y )
{
	switch( key )
	{
		case ESC:
			deinitialize( );
			exit( 0 );
			break;
		case 'P':
		case 'p':
				for( int i = 0; i < COUNT; i++ ) pEntity[ i ]->findPath( );
				break;
		case 'C':
		case 'c':
			{
			
			}
			break;
		case 'R':
		case 'r':
			for( int i = 0; i < COUNT; i++ ) pEntity[ i ]->reset( );
			break;
		default:
			break;
	}

}

void idle( )
{ glutPostRedisplay( ); }

void writeText( void *font, std::string &text, int x, int y, float r, float g, float b )
{
	int width = glutGet( (GLenum) GLUT_WINDOW_WIDTH );
	int height = glutGet( (GLenum) GLUT_WINDOW_HEIGHT );

	glPushAttrib( GL_LIGHTING_BIT | GL_TEXTURE_BIT | GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT );
		glDisable( GL_DEPTH_TEST );
		glDisable( GL_TEXTURE_2D );
		glDisable( GL_LIGHTING );

		glMatrixMode( GL_PROJECTION );
		glPushMatrix( );
			glLoadIdentity( );	
			glOrtho( 0, width, 0, height, 1.0, 10.0 );
				
			glMatrixMode( GL_MODELVIEW );
			glPushMatrix( );
				glLoadIdentity( );
				glColor3f( r, g, b );
				glTranslatef( 0.0f, 0.0f, -1.0f );
				glRasterPos2i( x, y );

				for( unsigned int i = 0; i < text.size( ); i++ )
					glutBitmapCharacter( font, text[ i ] );
				
			glPopMatrix( );
			glMatrixMode( GL_PROJECTION );
		glPopMatrix( );
		glMatrixMode( GL_MODELVIEW );
	glPopAttrib( );
}


/////////////////////////////////////////////////////////////////////////////////
/////////////////////// NAVIGAION MESH //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
NavigationMesh *NavigationMesh::pInstance = NULL;

NavigationMesh *NavigationMesh::instance( )
{
	if( !pInstance )
	{
		pInstance = new NavigationMesh( );
	}

	return pInstance;
}

NavigationMesh::NavigationMesh( ) 
  : nodes( )
{
}

NavigationMesh::~NavigationMesh( )
{
}

void NavigationMesh::add( const NavigationMesh::Polygon &p )
{
	Node node;
	node.p = p;
	nodes.push_back( node );

}

void NavigationMesh::initialize( )
{
	// For each polygon P
	for( int i = 0; i < nodes.size( ); i++ )
	{

		calculateCenter( i );
		determineConnectivity( i );
	}
}


bool NavigationMesh::Edge::operator==( const Edge &e )
{
	#if EDGES_EQUAL_IF_POINTS_ARE_CLOSE	
	OBJLoader::OBJGroupCollection &data  = objLoader.mesh( );
	OBJLoader::OBJMeshVertex &thisVert1  = data[ 0 ].mesh[ vertexIndex1 ];
	OBJLoader::OBJMeshVertex &otherVert1 = data[ 0 ].mesh[ e.vertexIndex1 ];
	OBJLoader::OBJMeshVertex &thisVert2  = data[ 0 ].mesh[ vertexIndex2 ];
	OBJLoader::OBJMeshVertex &otherVert2 = data[ 0 ].mesh[ e.vertexIndex2 ];

	bool sameVert1 = sqrt( (thisVert1.x - otherVert1.x)*(thisVert1.x - otherVert1.x) + (thisVert1.y - otherVert1.y)*(thisVert1.y - otherVert1.y) + (thisVert1.z - otherVert1.z)*(thisVert1.z - otherVert1.z) ) < 0.001f;
	bool sameVert2 = sqrt( (thisVert2.x - otherVert2.x)*(thisVert2.x - otherVert2.x) + (thisVert2.y - otherVert2.y)*(thisVert2.y - otherVert2.y) + (thisVert2.z - otherVert2.z)*(thisVert2.z - otherVert2.z) ) < 0.001f;

	return sameVert1 && sameVert2;
	#else 	
	return (vertexIndex1 == e.vertexIndex1 && vertexIndex2 == e.vertexIndex2 ) || 
	       (vertexIndex1 == e.vertexIndex2 && vertexIndex2 == e.vertexIndex1 );
	#endif
}

void NavigationMesh::calculateCenter( int nodeId )
{
	Node &node = nodes[ nodeId ];
	// The center is calculated by taking the average of each component.
	node.center.x = 0.0f;
	//node.center.y = 0.0f;
	node.center.z = 0.0f;

	const OBJLoader::OBJVertexCollection &verts = objLoader.vertices( );
	unsigned int numberOfVerticesInPolygon = node.p.vertices.size( );
	Debug::panicIfFalse( numberOfVerticesInPolygon >= 3, "[NavigationMesh] It needs to be a polygon!" );

#if _NOT_CORRECT
	for( int i = 0; i < numberOfVerticesInPolygon; i++ )
	{
		const OBJLoader::OBJVertex &v = verts[ node.p.vertices[ i ] ];

		node.center.x += v.x;
		//node.center.y += v.y;
		node.center.z += v.z;
	}

	if( numberOfVerticesInPolygon > 0 )
	{
		node.center.x /= numberOfVerticesInPolygon; 
		//node.center.y /= numberOfVerticesInPolygon;
		node.center.z /= numberOfVerticesInPolygon;
	}
#else
	float area = 0.0f;
	for( int i = 0; i < numberOfVerticesInPolygon; i++ )
	{
		const OBJLoader::OBJVertex *v = &verts[ node.p.vertices[ i ] ];
		const OBJLoader::OBJVertex *vNext = NULL;
		
		if( i + 1 < numberOfVerticesInPolygon )
		{
			vNext = &verts[ node.p.vertices[ i + 1 ] ];
		}
		else
		{
			vNext = &verts[ node.p.vertices[ 0 ] ];
		}

		area += ( v->x * vNext->z - vNext->x * v->z );
		node.center.x += ( v->x + vNext->x ) * ( v->x * vNext->z - vNext->x * v->z );
		node.center.z += ( v->z + vNext->z ) * ( v->x * vNext->z - vNext->x * v->z );
	}

	area *= 0.5f;
	node.center.x /= (6.0f * area);
	node.center.z /= (6.0f * area);

#endif
	// This might look silly, but it ensures we have the correct polygon winding defined.
	Debug::panicIfFalse( isPointInside( nodeId, node.center.x, node.center.z ), "[NavigationMesh] The center is not inside the polygon!" );
}

void NavigationMesh::determineConnectivity( int nodeId ) // O( v^2 )
{
	Node &node = nodes[ nodeId ];
	// An optimization can occur here.
	// We will always have two polygons that share an edge.
	// When we find a shared edge we add it to both nodes, and 
	// add the second to an exclude list so it can be skipped.

	// For each edge of P
	for( int j = 0; j < node.p.edges.size( ); j++ ) 
	{
		Edge &e = node.p.edges[ j ];

		// Is this edge shared in another Polygon?	
		for( int k = 0; k < nodes.size( ); k++ )
		{
			if( nodeId == k ) continue; // skip over Polygon P
			Node &otherNode = nodes[ k ];

			for( int l = 0; l < otherNode.p.edges.size( ); l++ )
			{
				if( e == otherNode.p.edges[ l ] )
				{
					node.connected.push_back( k );
					//node.sharedEdges.push_back( e ); // save the shared edge for corner detection
					node.sharedEdges[ k ] = j; // save the shared edge for corner detection
				}
			}
		}
	}
}

const NavigationMesh::Polygon& NavigationMesh::nodePolygon( int nodeId ) const
{	
	Debug::panicIfFalse( nodeId >= 0, "[NavigationMesh] The nodeId is not in the 0 <= nodeId <= SIZE" );
	Debug::panicIfFalse( nodeId < nodes.size( ), "[NavigationMesh] The nodeId is not in the 0 <= nodeId < SIZE" );
	return nodes[ nodeId ].p;
}

void NavigationMesh::getNodeCenter( int nodeId, float &x, float &z ) const
{
	Debug::panicIfFalse( nodeId >= 0, "[NavigationMesh] The nodeId is not in the 0 <= nodeId <= SIZE" );
	Debug::panicIfFalse( nodeId < nodes.size( ), "[NavigationMesh] The nodeId is not in the 0 <= nodeId < SIZE" );

	x = nodes[ nodeId ].center.x;
	z = nodes[ nodeId ].center.z;
}

void NavigationMesh::getNodeEdgeMidpoint( int nodeId, int edge, float &x, float &z ) const
{
	Debug::panicIfFalse( nodeId >= 0, "[NavigationMesh] The nodeId is not in the 0 <= nodeId <= SIZE" );
	Debug::panicIfFalse( nodeId < nodes.size( ), "[NavigationMesh] The nodeId is not in the 0 <= nodeId < SIZE" );

	const Node &n = nodes[ nodeId ];
	const Edge &e = n.p.edges[ edge ];

	const OBJLoader::OBJVertexCollection &verts = objLoader.vertices( );
	const OBJLoader::OBJVertex &v1 = verts[ e.vertexIndex1 ];
	const OBJLoader::OBJVertex &v2 = verts[ e.vertexIndex2 ];

	x = 0.5f * ( v1.x + v2.x );
	z = 0.5f * ( v1.z + v2.z );

}

bool NavigationMesh::isPointInside( int nodeId, float x, float z ) const
{
	Debug::panicIfFalse( nodeId >= 0, "[NavigationMesh] The nodeId is not in the 0 <= nodeId <= SIZE" );
	Debug::panicIfFalse( nodeId < nodes.size( ), "[NavigationMesh] The nodeId is not in the 0 <= nodeId < SIZE" );

	const Node &n = nodes[ nodeId ];
	const OBJLoader::OBJVertexCollection &verts = objLoader.vertices( );

	// we consider points on the edges of the polygon to be outside
	for( int i = 0; i < n.p.edges.size( ); i++ )
	{
		const Edge &e = n.p.edges[ i ];
		const OBJLoader::OBJVertex &vert1 = verts[ e.vertexIndex1 ];
		const OBJLoader::OBJVertex &vert2 = verts[ e.vertexIndex2 ];
		const float descriminator = (z - vert1.z) * (vert2.x - vert1.x) - (x - vert1.x) * (vert2.z - vert1.z);

		// this alogrithm depends on the polygon winding.
		#ifdef POLYGONS_DEFINED_CLOCKWISE
		if( descriminator >= 0 ) 
			return false;
		#else // counter-clockwise
		if( descriminator <= 0 ) 
			return false;
		#endif
	}

	// I think..
	// it would suffice to only check the number of edges - 1
	// but I would have to show a mathematical proof that I 
	// don't have the time for. 
	return true;
}

bool NavigationMesh::getEdgeIndexAlongNeighbor( int nodeId, int neigborId, int &sharedEdge ) const
{
	Debug::panicIfFalse( nodeId >= 0, "[NavigationMesh] The nodeId is not in the 0 <= nodeId <= SIZE" );
	Debug::panicIfFalse( nodeId < nodes.size( ), "[NavigationMesh] The nodeId is not in the 0 <= nodeId < SIZE" );

	const Node &n        = nodes[ nodeId ];
	const Node &neighbor = nodes[ neigborId ];

	SharedEdgesCollection::const_iterator itr = n.sharedEdges.find( neigborId );

	if( itr == n.sharedEdges.end( ) )
		return false;

	sharedEdge = itr->second;
	return true;
}


bool NavigationMesh::find( int startNode, int endNode )
{
	bool result = searchAlgorithm.find( endNode, startNode );
	
	if( result )
	{
		path = searchAlgorithm.getPath( );
	}

	return result;
}

bool NavigationMesh::hasNext( ) const
{
	return path != NULL;
}

int NavigationMesh::nextNode( )
{
	int nextNode = path->state;
	path = path->parent;
	return nextNode;
}

void NavigationMesh::releasePath( )
{
	searchAlgorithm.cleanup( );
	path = NULL;
}

////////////////////////////////////////////////////////////////////////
//////////////////////////// ENTITY ////////////////////////////////////
////////////////////////////////////////////////////////////////////////
const float Entity::MAX_SPEED = 5.0f;
const float Entity::RADIUS = 0.3f;

Entity::Entity( )
 : position(0,0), velocity(0,0)
{
	reset( );
	memset( color, 0, 3 * sizeof(float) );
	color[ rand() % 3 ] = Math::uniformBetween( 0.7f, 1.0f );
}

void Entity::reset( )
{
	path.clear( );
	startNode = rand( ) % NavigationMesh::instance( )->numberOfNodes( );

	float x, z;
	pNavMesh->getNodeCenter( startNode, x, z );

	position.X = x + Math::uniformBetween( -0.25f, 0.25f );
	position.Y = z + Math::uniformBetween( -0.25f, 0.25f );

	pathIndex = 0;
}

void Entity::findPath( )
{
	NavigationMesh *pNavMesh = NavigationMesh::instance( );

	if( pNavMesh->find( startNode, endNode ) )
	{
		path.clear( );
		while( pNavMesh->hasNext( ) )
		{
			path.push_back( pNavMesh->nextNode( ) );
			assert( path[ 0 ] == startNode );
		}	

		pNavMesh->releasePath( );
	}
}


void Entity::render( )
{
	glPushMatrix( );
		glColor3fv( color );
		glTranslatef( position.X, 1.0f, position.Y );
		gluSphere( pQuadric, RADIUS, 4, 4 );
	glPopMatrix( );
}

void Entity::update( float time )
{
	// follow path
	if( pathIndex < path.size( ) )
	{
		int sharedEdge = -1;	
		float x, z;

		if( pathIndex + 1 < path.size( ) )
		{
			bool foundSharedEdge = pNavMesh->getEdgeIndexAlongNeighbor( path[ pathIndex ], path[ pathIndex + 1 ], sharedEdge );
			Debug::panicIfFalse( foundSharedEdge, "Couldn't find a shared edge!" );

			pNavMesh->getNodeEdgeMidpoint( path[ pathIndex ], sharedEdge, x, z );
		}
		else
		{
			pNavMesh->getNodeCenter( endNode, x, z );
		}

		Vector2 goal( x + Math::uniformBetween(0.0f, 0.5f), z + Math::uniformBetween(0.0f, 0.5f) );

		if( (goal - position).length( ) < 0.4f )
		{
			pathIndex++;
		}
		else
		{			
			velocity += seek( goal ) * 0.7f;
			velocity += separate( ) * 0.3f;
			//velocity += cohesion( );
			position += velocity * time;

			nonPenetrationConstraint( );
		}
	}
}

Vector2 Entity::seek( const Vertex2 &goal ) const
{
	Vector2 desiredVelocity( goal - position );

	assert( !desiredVelocity.isZero( ) );
	desiredVelocity.normalize( );
	desiredVelocity *= MAX_SPEED;

	return (desiredVelocity - velocity);
	
}

Vector2 Entity::separate( ) const
{
	Vector2 force( 0, 0 );

	for( int i = 0; i < COUNT; i++ )
	{
		if( pEntity[ i ] == this ) continue;

		Vector2 pushDirection = position - pEntity[ i ]->position;
		
		assert( !pushDirection.isZero( ) );
		pushDirection.normalize( );
		
		float distance = ( position - pEntity[ i ]->position ).length( );

		force += pushDirection / distance;
	}

	return force;
}

Vector2 Entity::cohesion( ) const
{
	Vector2 center( 0, 0 );
	int neighbors = 0;

	for( int i = 0; i < COUNT; i++ )
	{
		if( pEntity[ i ] == this ) continue;
		center += position;
		neighbors++;
	}

	if( neighbors > 0 )
	{
		// TO DO
		//center /= neighbors;
		center /= neighbors;
		return seek( center );
	}
	else
	{
		return Vector2( 0, 0 );
	}
}


void Entity::nonPenetrationConstraint( )
{
	for( int i = 0; i < COUNT; i++ )
	{
		if( pEntity[ i ] == this ) continue;
		
		Vector2 toAgent( position - pEntity[ i ]->position );
		float distance = toAgent.length( );

		float overlap = 2 * RADIUS - distance;

		if( overlap > 0.0f )
		{
			position += (toAgent / distance) * overlap;
		}
	}
}
