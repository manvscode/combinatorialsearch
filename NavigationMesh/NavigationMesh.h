#ifndef _NAVIGATIONMESH_H_
#define _NAVIGATIONMESH_H_
/*
 *	NavigationMesh.h
 *
 *	3D pathfinding using a navigation mesh.
 *
 *	Coded by Joseph A. Marrero
 *	1/17/2009
 */
#include <string>
#include <vector>
#include <list>
#include <map>

#include <BestFirstSearch.h>
#include <AStarSearch.h>
#include <Vector2.h>
using namespace std;
using namespace Math;

#define POLYGONS_DEFINED_CLOCKWISE // polygons are defined clockwise...
#define ESC			27
#define GRID_CELL_WIDTH  20
#define GRID_CELL_HEIGHT 20

void initialize( );
void deinitialize( );

void render( );
void resize( int width, int height );
void keyboard_keypress( unsigned char key, int x, int y );
void idle( );
void writeText( void *font, std::string &text, int x, int y, float r = 1.0f, float g = 1.0f, float b = 1.0f );



class NavigationMesh
{
  protected:
	struct Edge {
		int vertexIndex1, vertexIndex2;
		Edge( int v1, int v2 ) : vertexIndex1(v1), vertexIndex2(v2) {}
		bool operator==( const Edge &e );
	};

  public:
	typedef std::vector<int>                  IndexCollection;
	typedef std::vector<NavigationMesh::Edge> EdgeCollection;
	class Polygon { // convex polygon
		friend class NavigationMesh;
		IndexCollection  vertices;
		EdgeCollection   edges;
	  public:
		Polygon( ){}
		void addVertex( int vertexIndex ) { vertices.push_back( vertexIndex ); }
		void addEdge( int vertexIndex1, int vertexIndex2 ) { edges.push_back( Edge( vertexIndex1, vertexIndex2 ) ); }
		int numberOfVertices( ) const { return vertices.size( ); }
		const IndexCollection& vertexIndices( ) const { return vertices; }

	};

	static NavigationMesh *instance( );
	~NavigationMesh( );


	void add( const NavigationMesh::Polygon &p );
	void initialize( );

	const NavigationMesh::Polygon &nodePolygon( int nodeId ) const;
	void getNodeCenter( int nodeId, float &x, float &z ) const;
	void getNodeEdgeMidpoint( int nodeId, int edge, float &x, float &z ) const;
	bool isPointInside( int nodeId, float x, float z ) const;
	bool getEdgeIndexAlongNeighbor( int nodeId, int neigborId, int &sharedEdge ) const;

	int numberOfNodes( ) const;


	bool find( int startNode, int endNode );
	bool hasNext( ) const;
	int nextNode( );
	void releasePath( );


  protected:
	typedef std::map<int, int> SharedEdgesCollection; // nodeId -> edge id in polygon


	struct Node {
		struct {
			float x, z;
		} center;
		Polygon p;
		IndexCollection connected; // indices into NodeCollection
		SharedEdgesCollection sharedEdges;
	};
	typedef std::vector<Node> NodeCollection;

	struct ManhattanDistance {
		float operator()( const int nodeId1, const int nodeId2 ) const
		{ 
			NavigationMesh *nm = NavigationMesh::instance( );
			const Node &n1 = nm->nodes[ nodeId1 ];
			const Node &n2 = nm->nodes[ nodeId2 ];
			return abs(n2.center.x - n1.center.x) + abs(n2.center.z - n1.center.z);
		}
	};

	struct DistanceSquared {
		float operator()( int nodeId1, int nodeId2 ) const
		{ 
			NavigationMesh *nm = NavigationMesh::instance( );
			const Node &n1 = nm->nodes[ nodeId1 ];
			const Node &n2 = nm->nodes[ nodeId2 ];

			int sharedEdge;
			nm->getEdgeIndexAlongNeighbor( nodeId1, nodeId2, sharedEdge );

			float mx, mz;
			nm->getNodeEdgeMidpoint( nodeId1, sharedEdge, mx, mz );

			return (n1.center.x - mx)*(n1.center.x - mx) + (n1.center.z - mz)*(n1.center.z - mz) +
			       (n2.center.x - mx)*(n2.center.x - mx) + (n2.center.z - mz)*(n2.center.z - mz);
		}
	};

	struct NodeSuccessors {
		const IndexCollection &operator()( int nodeId ) const
		{			 
			NavigationMesh *nm = NavigationMesh::instance( );
			const Node &n = nm->nodes[ nodeId ];
			return n.connected;
		}
	};

	typedef CombinatorialSearch::BestFirstSearch<int, DistanceSquared, NodeSuccessors> BFS;
	typedef CombinatorialSearch::AStarSearch<int, ManhattanDistance, DistanceSquared, NodeSuccessors> ASS;


	static NavigationMesh *pInstance;
	NodeCollection nodes;
	IndexCollection calculatedPath;
	ASS searchAlgorithm;
	ASS::Node *path;

  protected:
  	NavigationMesh( );
	void calculateCenter( int nodeId );
	void determineConnectivity( int nodeId );


};


inline int NavigationMesh::numberOfNodes( ) const
{
	return nodes.size( );
}

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
class Entity
{
  public:
	static const float MAX_SPEED;
	static const float RADIUS;
	Vector2 position;
	Vector2 velocity;
	NavigationMesh::IndexCollection path;
	int pathIndex;

	float color[ 3 ];

  protected:

	Vector2 seek( const Vertex2 &goal ) const;
	Vector2 separate( ) const;
	Vector2 cohesion( ) const;
	void nonPenetrationConstraint( );

  public:
	int startNode;
	Entity( );

	void findPath( );
	void reset( );	
	void render( );	
	void update( float time );
};

#endif
