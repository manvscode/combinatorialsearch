#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include "debug.h"

#ifdef WIN32 // Windows
#include <io.h>
#include <fcntl.h>
#include <windows.h>
#pragma comment(lib, "kernel32.lib")
#define GREY           7
#define GREEN          10
#define RED            12

#define SET_COLOR( ioHandle, color ) \
	SetConsoleTextAttribute( ioHandle, color )

#define NORMAL_COLOR   SET_COLOR( GetStdHandle( STD_OUTPUT_HANDLE ), GREY )
#define PANIC_COLOR    SET_COLOR( GetStdHandle( STD_OUTPUT_HANDLE ), RED )
#define DEBUG_COLOR    SET_COLOR( GetStdHandle( STD_OUTPUT_HANDLE ), GREEN )
#else // Linux
#error "Not implemented yet!"
#endif

#ifdef _DEBUG
namespace Debug
{
	#ifdef WIN32
	BOOL WINAPI __consoleHandler( DWORD dwCtrlType )
	{
		switch( dwCtrlType )
		{
			case CTRL_C_EVENT:
			case CTRL_BREAK_EVENT:
			case CTRL_CLOSE_EVENT:
			case CTRL_LOGOFF_EVENT:
			case CTRL_SHUTDOWN_EVENT:
				Debug::deinitialize( );
				break;
			default:
				break;
		}
		return TRUE;
	}
	#endif


	bool initialize( const char *title )
	{
		#ifdef WIN32
		int result            = AllocConsole( );
		HANDLE hConsoleStdOut = GetStdHandle( STD_OUTPUT_HANDLE );
		HANDLE hConsoleStdIn  = GetStdHandle( STD_INPUT_HANDLE );
		HANDLE hConsoleStdErr = GetStdHandle( STD_ERROR_HANDLE );


		int fdStdOut = _open_osfhandle( (intptr_t) hConsoleStdOut, _O_TEXT );
		int fdStdIn  = _open_osfhandle( (intptr_t) hConsoleStdIn, _O_TEXT );
		int fdStdErr = _open_osfhandle( (intptr_t) hConsoleStdErr, _O_TEXT );

		SET_COLOR( GetStdHandle( STD_ERROR_HANDLE ), RED );

		FILE *consoleOut = fdopen( fdStdOut, "wb" );
		FILE *consoleIn  = fdopen( fdStdOut, "rb" );
		FILE *consoleErr = fdopen( fdStdOut, "wb" );

		setvbuf( consoleIn, NULL, _IONBF, 0 );
		setvbuf( consoleOut, NULL, _IONBF, 0 );
		setvbuf( consoleErr, NULL, _IONBF, 0 );

		*stdin  = *consoleIn;
		*stdout = *consoleOut;
		*stderr = *consoleErr;

		SetConsoleCtrlHandler( __consoleHandler, true );

		SetConsoleTitle( title );

		CONSOLE_SCREEN_BUFFER_INFO coninfo;
		GetConsoleScreenBufferInfo( hConsoleStdOut, &coninfo );
		coninfo.dwSize.Y = 1024; // max console lines
		SetConsoleScreenBufferSize( hConsoleStdOut, coninfo.dwSize );

		NORMAL_COLOR;

		return result != 0;
		#else		
		return true;
		#endif	
	}

	bool deinitialize( )
	{
		#ifdef WIN32
		return FreeConsole( ) != 0;
		#else
		return true;
		#endif
	}

	void panic( const char *message, ... )
	{
		PANIC_COLOR;
		va_list args;
		va_start( args, message );
		vfprintf( stderr, message, args );
		va_end( args );
		abort( );
	}

	void panicIfFalse( bool test, const char *message, ... )
	{
		if( !test )
		{
			PANIC_COLOR;
			va_list args;
			va_start( args, message );
			vfprintf( stderr, message, args );
			va_end( args );
			abort( );
		}
	}

	void info( const char *message, ... )
	{
		DEBUG_COLOR;
		va_list args;
		va_start( args, message );
		vfprintf( stdout, message, args );
		va_end( args );
		NORMAL_COLOR;
	}

	void print( const char *message, ... )
	{
		NORMAL_COLOR;
		va_list args;
		va_start( args, message );
		vfprintf( stdout, message, args );
		va_end( args );
	}
} // end of namespace Debug
#endif
