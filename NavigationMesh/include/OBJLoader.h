#ifndef _OBJLOADER_H_
#define _OBJLOADER_H_
/*
 *	OBJLoader.h
 *
 *	An OBJ model loader.
 *
 *	Coded by Joseph A. Marrero. 12/22/06
 *	Modified 1/17/08
 *	http://www.ManVsCode.com/
 */
#include <fstream>
#include <string>
#include <vector>
#include <map>

class OBJLoader
{
  public:
	typedef float       OBJScalar;
	typedef std::string OBJString;

	typedef struct tagOBJVertex {
		OBJScalar x; OBJScalar y; OBJScalar z;
	} OBJVertex;

	typedef struct tagOBJTextureCoord {
		OBJScalar u; OBJScalar v;
	} OBJTextureCoord;

	typedef struct tagOBJNormal {
		OBJScalar nx; OBJScalar ny; OBJScalar nz;
	} OBJNormal;

	typedef std::vector<unsigned int> IndexCollection;	

	typedef struct tagOBJFace {
		IndexCollection vertexIndices; 
		IndexCollection textureIndices;
		IndexCollection normalIndices;
	} OBJFace;

	typedef struct tagOBJMeshVertex { // can be used with GL_T2F_N3F_V3F
		union {
			struct { OBJScalar u, v; };
			OBJTextureCoord textureCoordinate;
		};
		union {
			struct { OBJScalar nx, ny, nz; };
			OBJNormal normal;
		};
		union {
			struct { OBJScalar x, y, z; };
			OBJVertex vertex;
		};
	} OBJMeshVertex;

	typedef struct tagOBJMeshFace {
		IndexCollection indices;
	} OBJMeshFace;

	typedef std::vector<OBJLoader::OBJMeshVertex> OBJMeshCollection;
	typedef std::vector<OBJLoader::OBJMeshFace>   OBJMeshFaceCollection;

	typedef struct tagOBJGroup {
		OBJString             groupName;
		OBJMeshCollection     mesh;
		OBJMeshFaceCollection faces;
	} OBJGroup;

	typedef std::vector<OBJLoader::OBJVertex>       OBJVertexCollection;
	typedef std::vector<OBJLoader::OBJTextureCoord> OBJTextureCoordCollection;
	typedef std::vector<OBJLoader::OBJNormal>       OBJNormalCollection;
	typedef std::vector<OBJLoader::OBJGroup>        OBJGroupCollection;
	typedef std::vector<OBJLoader::OBJFace>         OBJFaceCollection;

  public:
	OBJLoader( );
	~OBJLoader( );

	bool load( const char *filename, bool verbose = false );
	void clear( );
	OBJGroupCollection&              mesh( );
	const OBJGroupCollection&        mesh( ) const;
	OBJVertexCollection&             vertices( );
	const OBJVertexCollection&       vertices( ) const;
	OBJTextureCoordCollection&       textureCoordinates( );
	const OBJTextureCoordCollection& textureCoordinates( ) const;
	OBJNormalCollection&             normals( );
	const OBJNormalCollection&       normals( ) const;
	OBJFaceCollection&               faces( );
	const OBJFaceCollection&         faces( ) const;

  protected:
	enum Element {
		ELM_VERTEX,
		ELM_TEXTURECOORD,
		ELM_NORMAL,
		ELM_FACE,
		ELM_GROUP,
		ELM_OTHER
	};

	void addDefaultGroup( );
	void setElement( char *token );
	void addVertex( );
	void addTextureCoord( );
	void addNormal( );
	void addFace( );
	void addGroup( );

	std::ifstream             m_File;
	Element                   m_CurrentElement;
	bool                      m_bVerboseOutput;
	OBJString                 m_CurrentGroup;
	OBJVertexCollection       m_Vertices;
	OBJTextureCoordCollection m_TextureCoords;
	OBJNormalCollection       m_Normals;
	OBJGroupCollection        m_Groups;
	OBJFaceCollection         m_Faces;
};

inline OBJLoader::OBJGroupCollection &OBJLoader::mesh( )
{ return m_Groups; }

inline const OBJLoader::OBJGroupCollection &OBJLoader::mesh( ) const
{ return m_Groups; }

inline OBJLoader::OBJVertexCollection &OBJLoader::vertices( )
{ return m_Vertices; }

inline const OBJLoader::OBJVertexCollection &OBJLoader::vertices( ) const
{ return m_Vertices; }

inline OBJLoader::OBJTextureCoordCollection &OBJLoader::textureCoordinates( )
{ return m_TextureCoords; }

inline const OBJLoader::OBJTextureCoordCollection &OBJLoader::textureCoordinates( ) const
{ return m_TextureCoords; }

inline OBJLoader::OBJNormalCollection &OBJLoader::normals( )
{ return m_Normals; }

inline const OBJLoader::OBJNormalCollection &OBJLoader::normals( ) const
{ return m_Normals; }

inline OBJLoader::OBJFaceCollection &OBJLoader::faces( )
{ return m_Faces; }

inline const OBJLoader::OBJFaceCollection &OBJLoader::faces( ) const
{ return m_Faces; }

#endif // _OBJLOADER_H_
