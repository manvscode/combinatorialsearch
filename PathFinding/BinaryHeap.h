#ifndef _BINARYHEAP_H_
#define _BINARYHEAP_H_

#include <memory>
#include <algorithm>
#include <functional>
#include <vector>
#include <unordered_set> //tr1

template <typename AnyType,
          typename Comparer = std::less<AnyType>,
		  typename Allocator = std::allocator<AnyType> >
class BinaryHeap
{
  protected:
	typedef std::vector<AnyType, Allocator> Collection;
	typedef std::unordered_set<AnyType> HashCollection;
	Collection c;
	HashCollection hc;
	Comparer comparer;

  public:
	typedef typename Collection::iterator iterator;
	typedef typename Collection::const_iterator const_iterator;



  public:
	BinaryHeap( )
		: c( )
	{
		heapify( );
	}

	BinaryHeap( std::vector<AnyType> &collection )
		: c( collection )
	{
	}

	AnyType &top( )
	{ return c.front( ); }

	void push( AnyType &t )
	{
		c.push_back( t );
		push_heap( c.begin( ), c.end( ), comparer );
	}

	void pop( )
	{
		pop_heap( c.begin( ), c.end( ), comparer );
		c.pop_back( );
	}

	typename Allocator::size_type size( ) const
	{ return c.size( ); }
	
	bool empty( ) const
	{ return c.empty( ); }

	void clear( )
	{ c.clear( ); }

  protected:
	void heapify( )
	{ std::make_heap( c.begin( ), c.end( ), comparer ); }

	
	iterator begin( )
	{ return c.begin( ); }

	iterator end( )
	{ return c.end( ); }

	const_iterator begin( ) const
	{ return c.begin( ); }

	const_iterator end( ) const
	{ return c.end( ); }



	// TO DO: Refactor this to use a hash table, to make it O(1) from O(n)
	iterator find( AnyType &t )
	{ return c.find( t ); }

	// TO DO: Refactor this to use a hash table, to make it O(1) from O(n)
	const_iterator find( AnyType &t ) const
	{ return c.find( t ); }


};


#endif

