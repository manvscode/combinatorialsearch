/*
 *	PathFinding.cc
 * 
 *	
 *
 *	Coded by Joseph A. Marrero
 *	1/26/2008
 */
#include <GL/freeglut/freeglut.h>
#include <cassert>
#include <iostream>
#include <sstream>
#include "PathFinding.h"
#define WIN32_LEAN_AND_MEAN
#include <windows.h>


using namespace std;

int windowWidth;
int windowHeight;
float tileWidth;
float tileHeight;
std::pair<int, int> start;
std::pair<int, int> end;
Tile *tiles = NULL;
unsigned int blockList = 0, gridList = 0;

GLfloat assPathColor[] = { 6.0f, 6.0f, 0.0f, 0.7f };
GLfloat bfsPathColor[] = { 0.0f, 6.0f, 6.0f, 0.7f };


unsigned int gridWidth = DEFAULT_gridWidth;
unsigned int gridHeight = DEFAULT_gridHeight;

int main( int argc, char *argv[] )
{
	MessageBox( NULL, "The left mouse button adds a blue wall, middle mouse button sets the start point, and the right mouse button to set the end point. Press <B> to trace a path found by best-first search, <A> to trace a path found by A* search.",
		"Instructions", MB_OK );

	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE );

	if( argc != 1 && (argc - 1) % 2 != 0 )
	{
		cout << "Usage: " << argv[ 0 ] << " -gw <Grid Width> -gh <Grid Height>" << endl << endl;
		cout << "The -gw and -gh options are optional. If they are omitted than the" << endl;
		cout << "defaults are used (" << gridWidth << " x " << gridHeight << ")." << endl;
		return 1;
	}


	for( int c = 1; c < argc; c += 2 )
	{
		if( strncmp( argv[ c ], "-gw", 3 ) == 0 )
			gridWidth = strtoul( argv[ c + 1 ], NULL, 0 );
		else if( strncmp( argv[ c ], "-gh", 3 ) == 0 )
			gridHeight = strtoul( argv[ c + 1 ], NULL, 0 );
	}


#ifndef FULLSCREEN
	glutInitWindowSize( 800, 600 );
	glutCreateWindow( "[Path Finding Demo] - http://www.ManVsCode.com/" );
#else
	glutGameModeString( "640x480" );

	if( glutGameModeGet( GLUT_GAME_MODE_POSSIBLE ) )
		glutEnterGameMode( );
	else
	{
		glutGameModeString( "640x480" );
		if( glutGameModeGet( GLUT_GAME_MODE_POSSIBLE ) )
			glutEnterGameMode( );
		else
		{
			cerr << "The requested mode is not available!" << endl;
			return -1;	
		}
	}
#endif


	glutDisplayFunc( render );
	glutReshapeFunc( resize );
	glutKeyboardFunc( keyboard_keypress );
	glutIdleFunc( idle );
	glutMouseFunc( mouse );
	glutMotionFunc( mouseMotion );


	glutSetOption( GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS );

	initialize( );
	glutMainLoop( );
	deinitialize( );

	return 0;
}


void initialize( )
{
	//glDisable( GL_ALPHA_TEST );
	glDisable( GL_DEPTH_TEST );
	
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
	//glEnable( GL_POLYGON_SMOOTH );
	glHint( GL_POINT_SMOOTH_HINT, GL_FASTEST );
	glHint( GL_LINE_SMOOTH_HINT, GL_FASTEST );
	//glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
	
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	

	glShadeModel( GL_SMOOTH );
	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );


	glPixelStorei( GL_PACK_ALIGNMENT, 4 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );


	// TO DO: Initialization code goes here...
	glEnable( GL_MAP2_VERTEX_3 );
	glEnable( GL_LINE_STIPPLE );


	tiles = new Tile[ gridWidth * gridHeight ];

	blockList = glGenLists( 2 );
	glNewList( blockList, GL_COMPILE );
		glBegin( GL_QUADS );
			glVertex2f( 0.0f, 0.0f );
			glVertex2f( 1.0f, 0.0f );
			glVertex2f( 1.0f, 1.0f );
			glVertex2f( 0.0f, 1.0f );
		glEnd( );
	glEndList( );


	gridList = blockList + 1;
	glNewList( gridList, GL_COMPILE );
		glPushAttrib( GL_CURRENT_BIT | GL_LINE_BIT );			
			glEnable( GL_LINE_STIPPLE );
			glLineStipple( 1, 0xF0F0 );
			glLineWidth( 0.35f );
			glColor3f( 0.2f, 1.0f, 1.0f );

			glMapGrid2f( gridWidth, 0.0, 1.0, gridHeight, 0.0, 1.0 );
			glEvalMesh2( GL_LINE, 0, gridWidth, 0, gridHeight );
		glPopAttrib( );
	glEndList( );

	reset( );
}

void deinitialize( )
{
	delete [] tiles;
	glDeleteLists( blockList, 2 );
}


void drawTiles( )
{
	// draw tiles
	for( unsigned int y = 0; y < gridHeight; y++ )
	{
		for( unsigned int x = 0; x < gridWidth; x++ )
		{
			int index = y * gridWidth + x ;

			glPushMatrix( );
				glPushAttrib( GL_CURRENT_BIT );
					if( x == start.first && y == start.second )
						glColor3f( 0.0f, 1.0f, 0.0f );
					else if( x == end.first && y == end.second )
						glColor3f( 1.0f, 0.0f, 0.0f );
					else if( !tiles[ index ].bWalkable )
						glColor3f( 0.0f, 0.0f, 1.0f );
					else
						continue;

					glTranslatef( x * tileWidth, y * tileHeight, 0.0f );
					glScalef( tileWidth, tileHeight, 1.0f );

					
					glCallList( blockList );

					//if( mode == MODE_BFS && tiles[ index ].cost != 0 )
					//{
					//	ostringstream oss;
					//	oss << "C: " << tiles[ index ].cost;
					//	writeText( GLUT_BITMAP_8_BY_13, oss.str( ), 0.15f * tileWidth + x * tileWidth, 0.15f * tileHeight + y * tileHeight );
					//}
					//else if( mode == MODE_ASTAR && tiles[ index ].F != 0 )
					//{
					//	ostringstream oss;
					//	oss << "F: " << tiles[ index ].F;
					//	writeText( GLUT_BITMAP_8_BY_13, oss.str( ), x * tileWidth, y *tileHeight );
					//}
				glPopAttrib( );
			glPopMatrix( );
		}
	}
}


void drawPath( const Path &p, GLfloat color[] )
{
	if( p.empty( ) ) return;

	glPushAttrib( GL_CURRENT_BIT | GL_LINE_BIT );		
		glColor4fv( color );

		glLineWidth( tileWidth / 5.0f );
		glDisable( GL_LINE_STIPPLE );
		glBegin( GL_LINE_STRIP );

		for( Path::const_iterator itr = p.begin( ); itr != p.end( ); ++itr )
		{
			Tile *pCurrent = *itr;
			glVertex2f( pCurrent->X * tileWidth + 0.5f * tileWidth, pCurrent->Y * tileHeight + 0.5f * tileHeight );
		}
		glEnd( );
	glPopAttrib( );
	
}

void render( )
{
	glClear( GL_COLOR_BUFFER_BIT  );
	glLoadIdentity( );	


	drawTiles( );

	drawPath( assPath, assPathColor );
	drawPath( bfsPath, bfsPathColor );

	// draw grid...
	if( gridWidth * gridWidth <= 1600 )
	{
		glCallList( gridList );
	}


	/* Write text */
	int width = glutGet((GLenum)GLUT_WINDOW_WIDTH);
	int height = glutGet((GLenum)GLUT_WINDOW_HEIGHT);

	writeText( GLUT_BITMAP_HELVETICA_18, std::string("Path Finding"), 2, 22 );
	writeText( GLUT_BITMAP_8_BY_13, std::string("Press <ESC> to quit, <A> for A*, <B> for Best-First Search, <R> to randomize, and <r> to reset."), 2, 5 );
	
	glutSwapBuffers( );
}

void resize( int width, int height )
{
	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );
	

	if( height == 0 )
		height = 1;
		
	glOrtho( 0.0, width, 0.0, height, 0.0, 1.0 );
	glMatrixMode( GL_MODELVIEW );

	windowWidth = width;
	windowHeight = height;


	// resize our grid...
	grid[ 0 ][ 1 ][ 0 ] = (GLfloat) width;
	grid[ 1 ][ 0 ][ 1 ] = (GLfloat) height;
	grid[ 1 ][ 1 ][ 0 ] = (GLfloat) width;
	grid[ 1 ][ 1 ][ 1 ] = (GLfloat) height;


	glMap2f( GL_MAP2_VERTEX_3, 0.0f, 1.0f, 3, 2, 0.0f, 1.0f, 2 * 3, 2, (GLfloat *) grid );

	// set tile dimensions...
	tileWidth = (float) width / (float) gridWidth;
	tileHeight = (float) height / (float) gridHeight;

}

void keyboard_keypress( unsigned char key, int x, int y )
{
	switch( key )
	{
		case ESC:
			deinitialize( );
			exit( 0 );
			break;
		case 'A':
		case 'a':
		{
			assPath.clear( );
			if( start.first == -1 || start.second == -1 ) return;
			if( end.first == -1 || end.second == -1 ) return;
			Tile *pStart = &tiles[ start.second * gridWidth + start.first ];
			Tile *pEnd = &tiles[ end.second * gridWidth + end.first ];

			bool bPathFound = ass.find( pStart, pEnd );

			if( bPathFound )
			{
				ASS::Node *pPathNode = ass.getPath( );

				while( pPathNode != NULL )
				{
					assPath.push_back( pPathNode->state );
					pPathNode = pPathNode->parent;
				}

			}
			
			ass.cleanup( );
			glutPostRedisplay( );
			break;
		}
		case 'B':
		case 'b':
		{ 
			bfsPath.clear( );
			if( start.first == -1 || start.second == -1 ) return;
			if( end.first == -1 || end.second == -1 ) return;
			Tile *pStart = &tiles[ start.second * gridWidth + start.first ];
			Tile *pEnd = &tiles[ end.second * gridWidth + end.first ];

			bool bPathFound = bfs.find( pStart, pEnd );

			if( bPathFound )
			{
				BFS::Node *pPathNode = bfs.getPath( );

				while( pPathNode != NULL )
				{
					bfsPath.push_back( pPathNode->state );
					pPathNode = pPathNode->parent;
				}

			}
			
			bfs.cleanup( );
			glutPostRedisplay( );
			break;
		}
		case 'R':
		{
			reset( true );
			glutPostRedisplay( );
			break;
		}
		case 'r':
		{
			reset( false );
			glutPostRedisplay( );
			break;
		}
		default:
			break;
	}

}

int mouseButton = 0;
int mouseButtonState = 0;

void mouse( int button, int state, int x, int y )
{
	mouseButton = button;
	mouseButtonState = state;

	if( mouseButton == GLUT_MIDDLE_BUTTON && mouseButtonState == GLUT_DOWN )
	{
		unsigned int elementX =  x / tileWidth;
		unsigned int elementY = (windowHeight - y) / tileHeight;
		
		start.first = elementX;
		start.second = elementY;
	}
	else if( mouseButton == GLUT_RIGHT_BUTTON && mouseButtonState == GLUT_DOWN )
	{
		unsigned int elementX =  x / tileWidth;
		unsigned int elementY = (windowHeight - y) / tileHeight;
		
		end.first = elementX;
		end.second = elementY;
	}
}

void mouseMotion( int x, int y )
{
	if( mouseButton == GLUT_LEFT_BUTTON && mouseButtonState == GLUT_DOWN )
	{
		unsigned int elementX =  x / tileWidth;
		unsigned int elementY = (windowHeight - y) / tileHeight;

		unsigned int index = elementY * gridWidth + elementX;
		tiles[ index ].bWalkable = false;
		glutPostRedisplay( );
	}
}

void idle( )
{ /*glutPostRedisplay( );*/ }

void writeText( void *font, std::string &text, int x, int y, float r, float g, float b )
{
	int width = glutGet( (GLenum) GLUT_WINDOW_WIDTH );
	int height = glutGet( (GLenum) GLUT_WINDOW_HEIGHT );

	glPushAttrib( GL_LIGHTING_BIT | GL_TEXTURE_BIT | GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT );
		glDisable( GL_DEPTH_TEST );
		glDisable( GL_TEXTURE_2D );
		glDisable( GL_LIGHTING );

		glMatrixMode( GL_PROJECTION );
		glPushMatrix( );
			glLoadIdentity( );	
			glOrtho( 0, width, 0, height, 1.0, 10.0 );
				
			glMatrixMode( GL_MODELVIEW );
			glPushMatrix( );
				glLoadIdentity( );
				glColor3f( r, g, b );
				glTranslatef( 0.0f, 0.0f, -1.0f );
				glRasterPos2i( x, y );

				for( unsigned int i = 0; i < text.size( ); i++ )
					glutBitmapCharacter( font, text[ i ] );
				
			glPopMatrix( );
			glMatrixMode( GL_PROJECTION );
		glPopMatrix( );
		glMatrixMode( GL_MODELVIEW );
	glPopAttrib( );
}



void reset( bool bRandomize )
{
	assPath.clear( );
	bfsPath.clear( );

	for( unsigned int y = 0; y < gridHeight; y++ )
	{
		for( unsigned int x = 0; x < gridWidth; x++ )
		{
			unsigned int index = y * gridWidth + x;

			if( bRandomize )
				tiles[ index ].bWalkable = ( rand() % 1000 ) > 200 ? true : false;
			else
				tiles[ index ].bWalkable =  true;

			tiles[ index ].X = x;
			tiles[ index ].Y = y;

			tiles[ index ].cost = 0;
			tiles[ index ].F = 0;
			tiles[ index ].G = 0;
			tiles[ index ].H = 0;
		}
	}

	start.first = -1;
	start.second = -1;
	end.first = -1;
	end.second = -1;
}

