#ifndef _ASTARPATHFINDING_H_
#define _ASTARPATHFINDING_H_
/*
 *	PathFinding.h
 *
 *	
 *
 *	Coded by Joseph A. Marrero
 *	1/26/2008
 */
#include <GL/gl.h>
#include <GL/freeglut/freeglut.h>
#include <string>
#include <utility>
#include <vector>
#include <cmath>

#include <CombinatorialSearch/AStarSearch.h>
#include <CombinatorialSearch/BestFirstSearch.h>
//#include "CombinatorialSearch/BreadthFirstSearch.h"
using namespace std;

#define ESC			27



#define DEFAULT_gridWidth			40		
#define DEFAULT_gridHeight			40

GLfloat grid[ 2 ][ 2 ][ 3 ] = {
		{ {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f} },
		{ {0.0f, 1.0f, 0.0f}, {1.0f, 1.0f, 0.0f} }
	};




typedef struct tagTile {
	unsigned int X;
	unsigned int Y;
	bool bWalkable;

	// stuff for BFS...
	int cost;

	// stuff for A*
	int F;
	int G;
	int H;

	bool operator==( const struct tagTile &other ) const
	{ return X == other.X && Y == other.Y; }

} Tile;

extern int windowWidth;
extern int windowHeight;
extern float tileWidth;
extern float tileHeight;

extern std::pair<int, int> start;
extern std::pair<int, int> end;

extern Tile *tiles;
extern unsigned int gridWidth;
extern unsigned int gridHeight;


void reset( bool bRandomize = false );

struct ManhattanDistance {
	unsigned int operator()( const Tile *t1, const Tile *t2 ) const
	{ return (unsigned int) std::abs( (int) (t1->X - t2->X) ) + std::abs( (int) (t1->Y - t2->Y) ); }
};

struct Distance {
	unsigned int operator()( const Tile *t1, const Tile *t2 ) const
	{ return (unsigned int) std::sqrt( (float) ((t1->X - t2->X) * (t1->X - t2->X) + (t1->Y - t2->Y) * (t1->Y - t2->Y)) ); }
};

struct TileSuccessors8 {
	std::vector<Tile *> operator()( const Tile *tile ) const
	{
		std::vector<Tile *> successors;

		for( int j = -1; j <= 1; j++ )
		{
			for( int i = -1; i <= 1; i++ )
			{
				if( i == 0 && j == 0 ) continue;
				int successorY = tile->Y + j;
				int successorX = tile->X + i;

				if( successorY < 0 ) continue;
				if( successorX < 0 ) continue;
				if( successorY >= gridWidth ) continue;
				if( successorX >= gridHeight ) continue;

				int index = successorY * gridWidth + successorX;

				if( tiles[ index ].bWalkable == false ) continue;

				successors.push_back( &tiles[ index ] );
			}
		}

		return successors;
	}
};


struct TileSuccessors4 {
	std::vector<Tile *> operator()( const Tile *tile ) const
	{
		std::vector<Tile *> successors;

		for( int i = -1; i <= 1; i += 2 )
		{
			int successorY = tile->Y;
			int successorX = tile->X + i;

			if( successorY < 0 ) continue;
			if( successorX < 0 ) continue;
			if( successorY >= gridHeight ) continue;
			if( successorX >= gridWidth ) continue;

			int index = successorY * gridWidth + successorX;

			if( tiles[ index ].bWalkable == false ) continue;

			successors.push_back( &tiles[ index ] );
		}

		for( int i = -1; i <= 1; i += 2 )
		{
			int successorY = tile->Y + i;
			int successorX = tile->X;

			if( successorY < 0 ) continue;
			if( successorX < 0 ) continue;
			if( successorY >= gridHeight ) continue;
			if( successorX >= gridWidth ) continue;

			int index = successorY * gridWidth + successorX;

			if( tiles[ index ].bWalkable == false ) continue;

			successors.push_back( &tiles[ index ] );
		}
		

		return successors;
	}
};


//typedef BreadthFirstSearch<Tile *, TileSuccessors> BFS;
typedef CombinatorialSearch::BestFirstSearch<Tile *, Distance, TileSuccessors4> BFS;
typedef CombinatorialSearch::AStarSearch<Tile *, Distance, ManhattanDistance, TileSuccessors4> ASS;
typedef vector<Tile *> Path;

ASS	ass;
BFS bfs;
Path assPath;
Path bfsPath;


void initialize( );
void deinitialize( );

void drawTiles( );
void drawPath( const Path &p, GLfloat color[] );
void render( );
void resize( int width, int height );
void keyboard_keypress( unsigned char key, int x, int y );
void mouse( int button, int state, int x, int y );
void mouseMotion( int x, int y );
void idle( );
void writeText( void *font, std::string &text, int x, int y, float r = 1.0f, float g = 1.0f, float b = 1.0f );



#endif
